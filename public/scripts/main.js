(function() {
	$(document).ready(function() {
		
	/* 
	 * Overlay
	 */
	var $overlay = $('#overlay'); 
		
	function loaded(){
		$(overlay).addClass( 'open' );
		$('.menu').addClass('active');
	}
	function toggleOpacity(){
		$(overlay).css('opacity',this.opacity);
		if(this.opacity == 0){
			loaded();
		}
	}
	function toggleSize(){
		var width = $(window).width() * (1.3-this.precentage);
		$(overlay).width(width).height(width);
	}
	function toggleOverlyAll(){
		if($(overlay).is('.open')){
			toggleSize.apply({precentage: 1});
			$(overlay).removeClass('open');
			$('.menu').addClass('active');
		}else{
			$(overlay).addClass('open');
			toggleSize.apply({precentage: -3});
		}
	}
	function toggleOverlay(precentage) {
		toggleSize.apply({precentage: precentage});
	}
		
		/* 
		 * Load image
		 */
		var win_width = Math.min($(window).width(),984);
		var win_height = $(window).height() -20;
		var resizeTimeout = 0;
		$('.gallery').hide();
		var count = 0, total = 104, slot = 10;
		var isLoadAll = false;
		var currentSlide = 0;
		var isListView = false;
		function loadImages(offset){
			if(offset > 0 && !isLoadAll && count < total){
				$('.chasing-dots').addClass('loading');
			}
			var loading = isLoadAll !== undefined && isLoadAll ? total-count : slot; 
			var postfix = isDisableOnePage ? 'md' : 'sm';
			for(var i= 1+offset; i<= total && i <= offset + loading; i++){
				var img= new Image();
				var data = {
					img: img,
					index: i
				}
				img.onload= loadCanvas.bind(data);
				
				img.src= 'public/img/6-29_'+i+'-'+postfix+'.jpg';
			}
		}
		/**
		 * Show photo when canvas are ready
		 */
		function loadFullPage(){
			$('#fullpage').fullpage({
				menu: '#menu',
				loopTop: false,
				loopBottom: true,
				scrollingSpeed: 1000,
				fitToSectionDelay: 500,
				setFitToSection: false,
				afterLoad: function(anchorLink, index){
					//console.log('[After Load] '+ index);
					if(!isListView){
						currentSlide = index;
					}
					if(index == 2){
						enableListViewButton();
					}
					if(index % 5 == 0 && count < total){
						loadImages(slot * Math.floor(index / 5));
					}
        }
			});
			if(win_width <= 640){
				disableOnePage();
			}
			$('.gallery').show();
		}
		function enableListViewButton(){
			$('.menu').addClass('enable-list-view');
		}
		/**
		 * Load canvas to specific section
		 */
		function loadCanvas(){
			var canvas = unscrambleImg(this.img,30,'ambrogio');
			var small_canvas = $(canvas).clone();
			var $canvas = $(resizeCanvas(canvas));
			var type = $canvas.is('.landscape') ? 'landscape': 'portrait';
			$('#section'+this.index+' .section-content').prepend($canvas);
			$('#section'+this.index).addClass(type);
			count++;
			if(count <= slot){
				toggleOverlay(count/total); 
				if(count == slot){
					$('.loading').removeClass('loading');
					toggleOverlyAll();
					loadFullPage();
				}	
			}else if(isLoadAll){
				toggleOverlay(.95-count/total); 
				if(count == total){
					toggleOverlay(-3); 
					$('body').removeClass('list-view-loading');
				}	
			}else if(count % slot ==0){
				$('.chasing-dots').removeClass('loading');
			}
		}
		/**
		 * Window resize handling
		 */
		function resize(){
			win_width = Math.min($(window).width(),984);
			win_height = $(window).height()-10;
			if(isDisableOnePage()){
				disableOnePage();
			}else{
				$.fn.fullpage.setAutoScrolling(true);
			}
			// Disable scroll when resize
			$('canvas').each(function(i,ele){
				resizeCanvas(ele);
			});
			$.fn.fullpage.reBuild();
    }
		/**
		 * Normal scrolling
		 */
		function disableOnePage(){
			$.fn.fullpage.setAutoScrolling(false);
			//$.fn.fullpage.destroy('all');
		}
		function isDisableOnePage(){
			return win_width <= 640;
		}
		/**
		 * Update canvas width / height
		 * @param   {dom} canvas 
		 * @returns {dom} Revised canvas
		 */
		function resizeCanvas(canvas){
			var $canvas = $(canvas);
			var $container = $(canvas).closest('.gallery');
			var landscapeRatio = win_width/canvas.width;
			var portraitRatio = win_height/canvas.height;
			var ratio = Math.min(landscapeRatio, portraitRatio);
			var translate50Ratio = -50 / ratio;
			var isLandscape = landscapeRatio == ratio;
			$canvas.removeClass('landscape portrait');
			$container.removeClass('landscape portrait');
			if(isLandscape){
				$canvas.addClass('landscape');
				$container.addClass('landscape');
			}else{
				$canvas.addClass('portrait');
				$container.addClass('portrait');
			}
			console.log(landscapeRatio,ratio,portraitRatio,isLandscape);
			//var windowRatio = !isLandscape ? -((( win_width - canvas.width * ratio) / 2) / canvas.width * ratio) * 100
			//: -(( win_height - canvas.height * ratio) / 2) / canvas.height * ratio;
			//windowRatio += translate50Ratio;
			var windowRatio = 0;
			var translate = (isLandscape ? translate50Ratio+'%, '+windowRatio+'%': windowRatio+'%, '+ translate50Ratio+'%'); 
			if(isDisableOnePage()){
				//ratio = landscapeRatio;
			}
			canvas.style.transform = 'scale('+ratio+') translate('+translate+')';
			
			return canvas;
		}
		function addListViewEvent(){
			$('#fullpage').on('click','.gallery',function(){
				if($('body').is('.list-view')){
					var slide = $(this).data('slide');
					toggleListView();
					$.fn.fullpage.silentMoveTo(slide+1);
					$.fn.fullpage.reBuild();
				}
			});	
		}
		function resizeCanvasList(canvas){
			var col = win_width/5 < 100 ? 3 : 5;
			var ratio = win_width/col/canvas.width;
			var translate = -50 / ratio;
			canvas.style.transform = 'scale('+ratio+')';
			return canvas;
		}
		/**
		 * Toggle List view
		 */
		function toggleListView(){
			var $body = $('body');
			$body.toggleClass('list-view');
			if($body.is('.list-view')){
				$body.addClass('list-view-loading');
				isListView = true;
				if(count < total){
					$overlay.addClass('open');
					isLoadAll = true;
				}else{
					isLoadAll = false;
					toggleOverlyAll();
					$body.removeClass('list-view-loading');
				}
				loadImages(count);
				$.fn.fullpage.setAutoScrolling(false);
			}else{
				isListView = false;
				toggleOverlyAll();
				$.fn.fullpage.setAutoScrolling(true);
				$.fn.fullpage.silentMoveTo(currentSlide);
				$.fn.fullpage.reBuild();
			}
		}
		$('header').on('click','.enable-list-view',function(evt){
			evt.preventDefault();
			toggleListView();
		});
		/* 
		 * Load Image first
		 */
		loadImages(0);
		addListViewEvent();
		$( window ).resize(function(){
			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(resize, 250);
    });
	});
})();