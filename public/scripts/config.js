require.config({
  shim: {

  },
  paths: {
    "async-waterfall": "bower_components/async-waterfall/index",
    "jquery.fullpage": "bower_components/fullpage.js/dist/jquery.fullpage",
    "jquery.easings": "bower_components/fullpage.js/vendors/jquery.easings.min",
    scrolloverflow: "bower_components/fullpage.js/vendors/scrolloverflow.min",
    "image-scramble": "bower_components/image-scramble/unscrambleImg",
    jquery: "bower_components/jquery/dist/jquery"
  },
  packages: [

  ]
});
require(['jquery'], function( $ ) {
    console.log( $ ) // OK
});