require.config({
  shim: {

  },
  paths: {
    "async-waterfall": "../public/scripts/bower_components/async-waterfall/index",
    classie: "../public/scripts/bower_components/classie/classie",
    "jquery.fullpage": "../public/scripts/bower_components/fullpage.js/dist/jquery.fullpage",
    "jquery.easings": "../public/scripts/bower_components/fullpage.js/vendors/jquery.easings.min",
    scrolloverflow: "../public/scripts/bower_components/fullpage.js/vendors/scrolloverflow.min",
    "image-scramble": "../public/scripts/bower_components/image-scramble/unscrambleImg",
    jquery: "../public/scripts/bower_components/jquery/dist/jquery",
    "Snap.svg": "../public/scripts/bower_components/Snap.svg/dist/snap.svg-min"
  },
  packages: [

  ]
});
