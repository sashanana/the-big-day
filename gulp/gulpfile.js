var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var responsive = require('gulp-responsive');
//var imagemin = require('gulp-imagemin');
var minify = require('gulp-minifier');
var plumber = require('gulp-plumber');
var nunjucksRender = require('gulp-nunjucks-render');
var data = require('gulp-data');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var wpPot = require('gulp-wp-pot');
var sort = require('gulp-sort');
var gulp = require('gulp')
  , wiredep = require('gulp-wiredep');
var imgScramble = require('gulp-image-scramble');
var resource = '../public/';
var server = require('gulp-server-livereload');
var rename = require("gulp-rename");
var htmlmin = require('gulp-htmlmin');

var plumberErrorHandler = { errorHandler: notify.onError({
 
    title: 'Gulp',
 
    message: 'Error: <%= error.message %>'
 
  })
 
};

gulp.task('sass-preprocess-clean', function(){
  
  var stream = gulp.src(resource+'css/sass/parts/_all.scss', {read: false})

    .pipe(clean({force: true}));
  
  return stream;
  
});
gulp.task('sass-preprocess-all', ['sass-preprocess-clean'],function(){
  // Create all.scss files from folder
  var stream = gulp.src(resource+'css/sass/parts/*.scss')

    .pipe(concat('_all.scss'))

    .pipe(gulp.dest(resource+'css/sass/parts/'));
  
  
  var stream = gulp.src(resource+'css/sass/sections/*.scss')

    .pipe(concat('_all.scss'))

    .pipe(gulp.dest(resource+'css/sass/sections/'));
  
  return stream;
  
});

gulp.task('sass', ['sass-preprocess-all'], function () {
  // Process Sass
  gulp.src(resource+'css/sass/[^_]*.scss')

    .pipe(plumber(plumberErrorHandler))

    .pipe(sass({
      includePaths: require('node-bourbon').includePaths
    }))

		.pipe(livereload())
	
    .pipe(gulp.dest(resource+'css'))

    .pipe(livereload());

});
gulp.task('js', function () {
 
    gulp.src(resource+'scripts/**/*.js')

    .pipe(livereload());
 
});
gulp.task('html', function () {
 
    gulp.src('../*.html')

    .pipe(livereload());
 
});
gulp.task('img', function() {
 
    gulp.src(resource+'6-29/*.JPG')
 
    .pipe(imagemin({
 
      optimizationLevel: 7,
 
      progressive: true
 
    }))
 
    .pipe(gulp.dest(resource+'img'));
		
		
		gulp.src(resource+'img/**/*.{png,jpg,gif}')
 
    .pipe(imagemin({
 
      optimizationLevel: 7,
 
      progressive: true
 
    }))
 
    .pipe(gulp.dest(resource+'img'))
  
    .pipe(livereload())
});

gulp.task('minify', ['sass-preprocess-all'], function() {
  /*    minify css   */
  gulp.src(resource+'css/sass/[^_]*.scss')
 
    .pipe(plumber(plumberErrorHandler))

    .pipe(sass({
      includePaths: require('node-bourbon').includePaths,
      outputStyle: 'compressed'
    }))

    .pipe(gulp.dest(resource+'css/min/'));
  
  /*    minify js    */ 
  
  gulp.src(resource+'scripts/main.js')

    .pipe(minify({
      minify: true,
      collapseWhitespace: true,
      conservativeCollapse: true,
      minifyJS: true
    }))
  
    .pipe(gulp.dest(resource+'scripts/min/'));
	
	/* minify html */
	gulp.src('../*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('../'))
  
});

gulp.task('bower', ['nunjucks'], function () {
  gulp.src('../index.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('../'))
  
    .pipe(livereload());
});

gulp.task('nunjucks', function () {
  // Gets .html and .nunjucks files in pages
  return gulp.src('../pages/**/*.+(html|nunjucks)')
	// Adding data to Nunjucks
	/*
	.pipe(data(function() {
		return require('../data.json')
	}))
	*/
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['../templates']
    }))
  // output files in app folder
  .pipe(gulp.dest('../'));

});

gulp.task('webserver', function() {
  gulp.src('../')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true,
      defaultFile: '../index.html'
    }));
});

gulp.task('img-responsive',function(){
	return gulp.src('../scramble-src-6/*.jpg')
    .pipe(responsive({
      // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
      '*.jpg': [{
        width: 640,
        rename: { suffix: '-sm' },
      }, {
        width: 984,
        rename: { suffix: '-md' },
      }, {
        width: 1760,
        rename: { suffix: '-lg' },
      }]
    }, {
      // Global configuration for all images
      // The output quality for JPEG, WebP and TIFF output formats
      quality: 100,
      // Use progressive (interlace) scan for JPEG and PNG output
      progressive: true,
      // Strip all metadata
      withMetadata: false,
    }))
    .pipe(gulp.dest('../scramble-src-slot'));
});

gulp.task('img-scramble', function(){
	return gulp.src('../scramble-src-6/*.jpg')
	.pipe(plumber(plumberErrorHandler))
	.pipe(imgScramble({
			seed:'ambrogio',
			sliceSize:30
	}))
	.pipe(gulp.dest(resource+'img/'))
});


gulp.task('watch', ['bower','sass','webserver'],function() {
 
    livereload.listen();
  
    gulp.watch(resource+'css/sass/**/*.scss', ['sass']);

    gulp.watch(resource+'scripts/**/*.js', ['js']);

    //gulp.watch(resource+'img/**/*.{png,jpg,gif}', ['img']);
  
    gulp.watch('../**/*.nunjucks', ['bower']);
    
    gulp.watch('./gulpfile.js', ['bower','sass']);
 
});
gulp.task('default', ['bower', 'minify']);